# Awesome Product Order REST API

REST Service for product management and placing product orders 

## Introduction

Awesome Product Order REST API is a sample application for product and order management.

## Recommended software

Project has been built on Spring Boot architecture and it utilizes [Project Lombok](https://projectlombok.org/) to autogenerate quite a lot of bytecode.
That's why an appropriate Lombok Plugin for IDE (see section *Recommended Software* for links ) is needed for smoot development.

But for project build the lombok does its job automatically so no additional config is necessary.

Recommended software to install:
1. [Java JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html), recommended the newest version.
2. [Apache Maven version 3.2.5](https://repo1.maven.org/maven2/org/apache/maven/apache-maven/3.2.5/) (newer one is problably fine as well) - for building the system and dependency management.
3. [GIT Version 2.20.1](https://git-scm.com/downloads) - Source Code Management(SCM) tool.
4. Lombok Plugin - [For IntelliJ](https://plugins.jetbrains.com/plugin/6317-lombok-plugin) or [Eclipse](https://howtodoinjava.com/automation/lombok-eclipse-installation-examples/)


## Technology stack

List of libraries and technologies used:

- Spring Boot
- Lombok
- Spring Data JPA
- H2 Database (file-based, no installation or configuration needed)
- Liquibase for DB Changeset Management
- JUnit 5 for testing
- Logback for logging
- Swagger for smooth REST API usage 

## System build and usage

In order to use the system, execute the command:

	mvn clean package spring-boot:run
	
Access the REST API via Swagger UI at [http://localhost:5000/swagger-ui.html](http://localhost:5000/swagger-ui.html).

## H2 Console and DB Access

H2 Database instance has a console available at [http://localhost:5000/h2-console](http://localhost:5000/h2-console)
	
	

