package com.gitlab.bialask.productorderservice.productorder.domain.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Data
@Builder
public class ProductModificationDTO {

	public ProductModificationDTO() {
	}

	public ProductModificationDTO(@NotEmpty
	                  @Size(max = 255)
			                  String name, @NotNull @Positive
			                  BigDecimal price) {
		this.name = name;
		this.price = price;
	}

	@NotEmpty
	@Size(max = 255)
	private String name;

	@NotNull
	@Positive
	private BigDecimal price;

}
