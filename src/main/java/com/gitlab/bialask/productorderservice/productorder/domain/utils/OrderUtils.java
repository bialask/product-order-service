package com.gitlab.bialask.productorderservice.productorder.domain.utils;

import com.gitlab.bialask.productorderservice.productorder.domain.model.Order;
import com.gitlab.bialask.productorderservice.productorder.domain.model.OrderItem;

import java.math.BigDecimal;

public class OrderUtils {

	public static BigDecimal calculateTotalPrice(Order order) {
		return order.getOrderItems().stream()
				.map(OrderItem::getTotal)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
	}


}
