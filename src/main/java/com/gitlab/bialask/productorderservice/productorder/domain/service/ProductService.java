package com.gitlab.bialask.productorderservice.productorder.domain.service;

import com.gitlab.bialask.productorderservice.productorder.domain.converter.ProductConverter;
import com.gitlab.bialask.productorderservice.productorder.domain.dto.ProductDTO;
import com.gitlab.bialask.productorderservice.productorder.domain.dto.ProductModificationDTO;
import com.gitlab.bialask.productorderservice.productorder.domain.model.Product;
import com.gitlab.bialask.productorderservice.productorder.domain.repository.ProductRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor(access = AccessLevel.PACKAGE, onConstructor = @__(@Autowired))
public class ProductService {

    private final ProductRepository productRepository;
    private final ProductConverter productConverter;

    @Transactional(readOnly = true)
    public ProductDTO getProductDto(long id) {
        Product product = productRepository
                .findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("The product with id %s has not been found!", id)));
        return productConverter.convert(product);
    }

    @Transactional(readOnly = true)
    public Product getProduct(long id) {
        return productRepository
                .findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("The product with id %s has not been found!", id)));
    }

    @Transactional(readOnly = true)
    public Collection<ProductDTO> findAllProducts() {
        return productRepository.findAll().stream()
                .map(productConverter::convert)
                .collect(Collectors.toList());
    }

    @Transactional
    public ProductDTO createProduct(ProductModificationDTO productDto) {
        Product product = productConverter.convert(productDto);
        Product savedProduct = productRepository.save(product);
        log.info("Product created: {}", savedProduct);
        return productConverter.convert(savedProduct);
    }

    @Transactional
    public ProductDTO updateProduct(Long id, ProductModificationDTO productDto) {
        Product product = productConverter.convert(productDto);
        product.setId(id);
        Product savedProduct = productRepository.save(product);
        log.info("Product updated: {}", savedProduct);
        return productConverter.convert(savedProduct);
    }

}
