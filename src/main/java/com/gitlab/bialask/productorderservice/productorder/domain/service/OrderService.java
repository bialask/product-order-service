package com.gitlab.bialask.productorderservice.productorder.domain.service;

import com.gitlab.bialask.productorderservice.productorder.domain.converter.OrderConverter;
import com.gitlab.bialask.productorderservice.productorder.domain.converter.ProductConverter;
import com.gitlab.bialask.productorderservice.productorder.domain.dto.*;
import com.gitlab.bialask.productorderservice.productorder.domain.model.Order;
import com.gitlab.bialask.productorderservice.productorder.domain.model.OrderItem;
import com.gitlab.bialask.productorderservice.productorder.domain.model.Product;
import com.gitlab.bialask.productorderservice.productorder.domain.repository.OrderRepository;
import com.gitlab.bialask.productorderservice.productorder.domain.utils.OrderUtils;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkArgument;

@Slf4j
@Service
@RequiredArgsConstructor(access = AccessLevel.PACKAGE, onConstructor = @__(@Autowired))
public class OrderService {

    private final OrderRepository orderRepository;
    private final ProductService productService;
    private final ProductConverter productConverter;
    private final OrderConverter orderConverter;


    @Transactional
    public OrderDTO createOrder(OrderCreationDTO orderCreationDTO) {
        Order order = convert(orderCreationDTO);
        final Order savedOrder = orderRepository.save(order);
        log.info("Order created with id:{}", savedOrder.getId());
        return orderConverter.convert(savedOrder);
    }

    @Transactional(readOnly = true)
    public OrderDTO getOrder(long id) {
        Order order = orderRepository
                .findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("The product with id %s has not been found!", id)));

        return orderConverter.convert(order);
    }

    @Transactional(readOnly = true)
    public List<OrderDTO> searchOrders(LocalDateTime from, LocalDateTime to) {
        final Collection<Order> orders = orderRepository.findByCreatedDateAfterAndCreatedDateBefore(from, to);
        return orders.stream()
                .map(orderConverter::convert)
                .collect(Collectors.toList());
    }

    private Order convert(OrderCreationDTO orderCreationDTO) {
        checkArgument(!orderCreationDTO.getProducts().isEmpty(), "There are no orderItems in the order!");
        Order order = orderConverter.convert(orderCreationDTO);
        order.setOrderItems(orderCreationDTO.getProducts().stream()
                .map(this::convert)
                .collect(Collectors.toList()));
        order.setTotalPrice(OrderUtils.calculateTotalPrice(order));
        return order;
    }

    private OrderItem convert(OrderCreationItemDTO orderCreationItemDTO) {
        Product product = productService.getProduct(orderCreationItemDTO.getProductId());
        return new OrderItem(orderCreationItemDTO.getQuantity(), product.getPrice(), product);
    }


    @Transactional
    public OrderDTO recalculateOrder(long id) {
        Order order = orderRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("The product with id %s has not been found!", id)));

        order.getOrderItems().forEach(OrderItem::updatePrice);
        order.setTotalPrice(OrderUtils.calculateTotalPrice(order));
        Order savedOrder = orderRepository.save(order);
        return orderConverter.convert(savedOrder);
    }
}
