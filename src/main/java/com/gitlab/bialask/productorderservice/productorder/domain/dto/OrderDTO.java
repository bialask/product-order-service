package com.gitlab.bialask.productorderservice.productorder.domain.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
public class OrderDTO {

    private Long id;
    private LocalDateTime createdDate;
    private String customerEmail;
    private BigDecimal totalPrice;
    private List<OrderItemDTO> items;

}
