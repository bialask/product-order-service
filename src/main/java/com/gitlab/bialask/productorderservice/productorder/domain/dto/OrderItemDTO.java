package com.gitlab.bialask.productorderservice.productorder.domain.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class OrderItemDTO {

	private Long productId;
	private String name;
	private BigDecimal price;
	private BigDecimal quantity;
}
