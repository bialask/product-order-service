package com.gitlab.bialask.productorderservice.productorder.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.io.Serializable;
import java.math.BigDecimal;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderItem implements Serializable {

    @NotNull
    @Positive
    @Column(nullable = false)
    private BigDecimal quantity;

    @NotNull
    @Positive
    @Column(name = "purchase_price", nullable = false)
    private BigDecimal purchasePrice;

    @ManyToOne
    @JoinColumn(name="product_id",foreignKey=@ForeignKey(name="fk_product_order_with_product"))
    private Product product;

    public BigDecimal getTotal() {
        return quantity.multiply(purchasePrice);
    }

    public void updatePrice() {
        this.purchasePrice = product.getPrice();
    }
}

