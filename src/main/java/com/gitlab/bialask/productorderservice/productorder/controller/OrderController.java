package com.gitlab.bialask.productorderservice.productorder.controller;


import com.gitlab.bialask.productorderservice.common.Constants;
import com.gitlab.bialask.productorderservice.exception.IllegalDateRangeException;
import com.gitlab.bialask.productorderservice.productorder.domain.dto.OrderCreationDTO;
import com.gitlab.bialask.productorderservice.productorder.domain.dto.OrderDTO;
import com.gitlab.bialask.productorderservice.productorder.domain.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(Constants.REST_BASE_PATH + "/order")
@Api(tags = {"Order resource"})
public class OrderController {
    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping
    @ApiOperation("Creates the new order with given order items (the latest product version is assigned to the order)")
    public OrderDTO createOrder(@RequestBody @Valid OrderCreationDTO orderCreateDTO) {
        return orderService.createOrder(orderCreateDTO);
    }

    @GetMapping("/{id}")
    @ApiOperation("Retrieve the order with all the order items")
    public OrderDTO getOrder(@PathVariable @ApiParam(value = "Order ID") long id) {
        return orderService.getOrder(id);
    }

    @PutMapping("/{id}/recalculate")
    @ApiOperation("Recalculate total order value based on current price of its order items")
    public OrderDTO recalculateOrder(@PathVariable @ApiParam(value = "Order ID") long id) {
        return orderService.recalculateOrder(id);
    }

    @GetMapping
    @ApiOperation("Search the orders by date between 'from' and 'to' dates.")
    public ResponseEntity<List<OrderDTO>> getOrders(
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
            @ApiParam(value = "Order Date From in format YYYY-MM-DDThh:mm:ss. Default: 7 days ago") Optional<LocalDateTime> dateFrom,
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) @RequestParam(required = false)
            @ApiParam(value = "Order Date To in format YYYY-MM-DDThh:mm:ss. Default: now") Optional<LocalDateTime> dateTo) {
        final LocalDateTime fromDate = dateFrom.orElse(LocalDateTime.now().minusDays(7));
        final LocalDateTime toDate = dateTo.orElse(LocalDateTime.now());

        if (toDate.isBefore(fromDate)) {
            throw new IllegalDateRangeException("The from date must be before date to");
        }
        final List<OrderDTO> ordersList = orderService.searchOrders(fromDate, toDate);
        return ResponseEntity.ok(ordersList);
    }

}
