package com.gitlab.bialask.productorderservice.productorder.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;

@Entity
@Table(name = "orderunit")
@EntityListeners(AuditingEntityListener.class)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Order implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotEmpty
    @Email
    @Column(name = "customer_email", nullable = false)
    private String customerEmail;
    @NotNull
    @Positive
    private BigDecimal totalPrice;
    @CreatedDate
    @Column(name = "created_date", nullable = false, updatable = false)
    private LocalDateTime createdDate;

    @NotEmpty
    @ElementCollection
    @CollectionTable(
            name = "product_order",
            joinColumns = @JoinColumn(name = "order_id")
    )
    private List<OrderItem> orderItems;

    @PrePersist
    private void onSave() {
        createdDate = LocalDateTime.now();
    }

}
