package com.gitlab.bialask.productorderservice.productorder.domain.converter;

import com.gitlab.bialask.productorderservice.productorder.domain.dto.OrderCreationDTO;
import com.gitlab.bialask.productorderservice.productorder.domain.dto.OrderDTO;
import com.gitlab.bialask.productorderservice.productorder.domain.dto.OrderItemDTO;
import com.gitlab.bialask.productorderservice.productorder.domain.model.Order;
import com.gitlab.bialask.productorderservice.productorder.domain.model.OrderItem;
import com.gitlab.bialask.productorderservice.productorder.domain.model.Product;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderConverter {

	public OrderDTO convert(Order order) {
		List<OrderItemDTO> orderItemList = order.getOrderItems().stream()
				.map(this::convert)
				.collect(Collectors.toList());
		return OrderDTO.builder()
				.id(order.getId())
				.customerEmail(order.getCustomerEmail())
				.createdDate(order.getCreatedDate())
				.totalPrice(order.getTotalPrice())
				.items(orderItemList).build();
	}

	public Order convert(OrderCreationDTO orderCreationDTO) {
		return Order.builder()
				.customerEmail(orderCreationDTO.getCustomerEmail())
				.build();
	}

	public OrderItemDTO convert(OrderItem orderItem) {
		Product product = orderItem.getProduct();
		return OrderItemDTO.builder()
				.productId(product.getId())
				.name(product.getName())
				.quantity(orderItem.getQuantity())
				.price(orderItem.getPurchasePrice()).build();
	}

}
