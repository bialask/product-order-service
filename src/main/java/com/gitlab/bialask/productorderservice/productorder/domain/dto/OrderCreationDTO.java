package com.gitlab.bialask.productorderservice.productorder.domain.dto;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderCreationDTO {

	@Email
	@NotEmpty
	private String customerEmail;

	@NotEmpty
	private List<OrderCreationItemDTO> products = Lists.newArrayList();

}
