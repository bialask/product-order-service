package com.gitlab.bialask.productorderservice.productorder.domain.repository;

import com.gitlab.bialask.productorderservice.productorder.domain.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
