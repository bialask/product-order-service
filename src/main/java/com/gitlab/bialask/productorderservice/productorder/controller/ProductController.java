package com.gitlab.bialask.productorderservice.productorder.controller;

import com.gitlab.bialask.productorderservice.common.Constants;
import com.gitlab.bialask.productorderservice.productorder.domain.dto.ProductModificationDTO;
import com.gitlab.bialask.productorderservice.productorder.domain.dto.ProductDTO;
import com.gitlab.bialask.productorderservice.productorder.domain.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@Slf4j
@RestController
@RequestMapping(Constants.REST_BASE_PATH + "/product")
@Api(tags = {"Product resource"})
@RequiredArgsConstructor(access = AccessLevel.PACKAGE, onConstructor = @__(@Autowired))
public class ProductController {

    private final ProductService productService;

    @GetMapping("/{id}")
    @ApiOperation("Get product by given id")
    public ProductDTO getProduct(@PathVariable @ApiParam(value = "Product ID") long id) {
        return productService.getProductDto(id);
    }

    @GetMapping
    @ApiOperation("Fetch all products")
    public Collection<ProductDTO> getAllProducts() {
        return productService.findAllProducts();
    }

    @PostMapping
    @ApiOperation("Create a product")
    public ProductDTO createProduct(@Valid @RequestBody @ApiParam(name = "user", value = "Product to be created")
                                                ProductModificationDTO product) {
        log.info("Create new product={}", product);
        return productService.createProduct(product);
    }

    @PutMapping("/{id}")
    @ApiOperation("Update the product by given id")
    public ProductDTO updateProduct(@PathVariable long id, @Valid @RequestBody
        @ApiParam(name = "user", value = "User to be updated") ProductModificationDTO product) {
        log.info("Update product={} for id={}", product, id);
        return productService.updateProduct(id, product);
    }


}
