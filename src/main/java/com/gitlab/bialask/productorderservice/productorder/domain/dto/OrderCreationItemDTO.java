package com.gitlab.bialask.productorderservice.productorder.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderCreationItemDTO {

	private Long productId;
	private BigDecimal quantity;
}
