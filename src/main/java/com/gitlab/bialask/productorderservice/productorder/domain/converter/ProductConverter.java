package com.gitlab.bialask.productorderservice.productorder.domain.converter;

import com.gitlab.bialask.productorderservice.productorder.domain.dto.ProductDTO;
import com.gitlab.bialask.productorderservice.productorder.domain.dto.ProductModificationDTO;
import com.gitlab.bialask.productorderservice.productorder.domain.model.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductConverter {

	public Product convert(ProductModificationDTO dto) {
		return Product.builder()
				.name(dto.getName())
				.price(dto.getPrice()).build();
	}

	public ProductDTO convert(Product product) {
		return ProductDTO.builder()
				.id(product.getId())
				.name(product.getName())
				.price(product.getPrice()).build();

	}
}
