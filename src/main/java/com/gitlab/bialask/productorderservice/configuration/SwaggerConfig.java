package com.gitlab.bialask.productorderservice.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

    @Bean
    public Docket storeApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(metadata())
		        .groupName("Product Order Awesome API")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.gitlab.bialask"))
                .build();
    }

    private ApiInfo metadata() {
        return new ApiInfoBuilder()
                .title("Product Order REST API" )
                .description("Awesome REST API for Product Management and Product Order Management")
                .version("1.0")
                .build();
    }


}
