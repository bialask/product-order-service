package com.gitlab.bialask.productorderservice.exception;

public class IllegalDateRangeException extends RuntimeException {

    public IllegalDateRangeException(String message) {
        super(message);
    }
}
