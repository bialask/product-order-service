package com.gitlab.bialask.productorderservice.productorder.domain.fixtures;

import com.gitlab.bialask.productorderservice.productorder.domain.dto.OrderCreationDTO;
import com.gitlab.bialask.productorderservice.productorder.domain.dto.OrderCreationItemDTO;
import com.google.common.collect.Lists;

import java.math.BigDecimal;
import java.util.List;

public class OrderFixture {

	public static String TEST_EMAIL = "tesla@gmail.com";
	public static BigDecimal TEST_PRICE = new BigDecimal(10.34F);

	public static OrderCreationDTO orderCreationWithTwoItems(Long firstProductId, BigDecimal firstItemQuantity,
	                                                  Long secondProductId, BigDecimal secondItemQuantity) {
		OrderCreationItemDTO firstItem = new OrderCreationItemDTO(firstProductId, firstItemQuantity);
		OrderCreationItemDTO secondItem = new OrderCreationItemDTO(secondProductId, secondItemQuantity);
		List<OrderCreationItemDTO> itemDTOList = Lists.newArrayList(firstItem, secondItem);
		return OrderCreationDTO.builder()
				.customerEmail(TEST_EMAIL)
				.products(itemDTOList).build();
	}
}
