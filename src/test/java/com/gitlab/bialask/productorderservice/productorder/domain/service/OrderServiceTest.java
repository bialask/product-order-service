package com.gitlab.bialask.productorderservice.productorder.domain.service;

import com.gitlab.bialask.productorderservice.productorder.domain.converter.OrderConverter;
import com.gitlab.bialask.productorderservice.productorder.domain.converter.ProductConverter;
import com.gitlab.bialask.productorderservice.productorder.domain.dto.OrderCreationDTO;
import com.gitlab.bialask.productorderservice.productorder.domain.dto.OrderDTO;
import com.gitlab.bialask.productorderservice.productorder.domain.dto.ProductDTO;
import com.gitlab.bialask.productorderservice.productorder.domain.dto.ProductModificationDTO;
import com.gitlab.bialask.productorderservice.productorder.domain.fixtures.OrderFixture;
import com.gitlab.bialask.productorderservice.productorder.domain.fixtures.ProductFixture;
import com.gitlab.bialask.productorderservice.productorder.domain.repository.OrderRepository;
import com.gitlab.bialask.productorderservice.productorder.domain.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ExtendWith(SpringExtension.class)
class OrderServiceTest {

	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private OrderRepository orderRepository;

	private ProductService productService;
	private OrderService orderService;

	private ProductDTO productOne;
	private ProductDTO productTwo;

	@BeforeEach
	void setUp() {
		productService = new ProductService(productRepository,new ProductConverter());
		productOne = productService.createProduct(ProductFixture.productModificationOne());
		productTwo = productService.createProduct(ProductFixture.productModificationTwo());

		orderService = new OrderService(orderRepository, productService, new ProductConverter(), new OrderConverter());
	}


	@Test
	void shouldCreateNewOrder() {
		// given
		BigDecimal firstItemQuantity = BigDecimal.TEN;
		BigDecimal secondItemQuantity = BigDecimal.ONE;
		OrderCreationDTO orderCreationDTO = OrderFixture.orderCreationWithTwoItems(productOne.getId(), firstItemQuantity,
				productTwo.getId(), secondItemQuantity);

		// when
		OrderDTO createdOrder = orderService.createOrder(orderCreationDTO);

		// then
		OrderDTO retrievedOrder = orderService.getOrder(createdOrder.getId());
		assertThat(createdOrder.getCustomerEmail()).isEqualTo(retrievedOrder.getCustomerEmail());
		assertThat(createdOrder.getCustomerEmail()).isEqualTo(orderCreationDTO.getCustomerEmail());
		assertThat(createdOrder.getItems().get(0).getName()).isEqualTo(productOne.getName());
		assertThat(createdOrder.getItems().get(0).getPrice()).isEqualTo(productOne.getPrice());
		assertThat(createdOrder.getTotalPrice()).isEqualTo((productOne.getPrice().multiply(firstItemQuantity)).add(productTwo.getPrice().multiply(secondItemQuantity)));
		assertThat(createdOrder.getCreatedDate()).isBefore(LocalDateTime.now());
	}

	@Test
	void shouldOrderRetainTotalPriceOnProductPriceUpdate() {
		// given
		OrderCreationDTO orderCreationDTO = OrderFixture.orderCreationWithTwoItems(productOne.getId(), BigDecimal.TEN,
				productTwo.getId(), BigDecimal.TEN);

		OrderDTO createdOrder = orderService.createOrder(orderCreationDTO);
		BigDecimal originalTotalPrice = createdOrder.getTotalPrice();

		// when
		productService.updateProduct(productOne.getId(), new ProductModificationDTO(productOne.getName(), new BigDecimal(10.5)));

		// then
		OrderDTO retrievedOrder = orderService.getOrder(createdOrder.getId());
		assertThat(retrievedOrder.getTotalPrice()).isEqualTo(originalTotalPrice);

	}

	@Test
	void shouldRecalculateOrderTotalPriceAfterProductPriceUpdate() {
		// given
		BigDecimal secondItemQuantity = BigDecimal.ONE;
		BigDecimal finalFirstItemQuantity = new BigDecimal(10.5);
		OrderCreationDTO orderCreationDTO = OrderFixture.orderCreationWithTwoItems(productOne.getId(), BigDecimal.TEN,
				productTwo.getId(), secondItemQuantity);

		OrderDTO createdOrder = orderService.createOrder(orderCreationDTO);
		BigDecimal originalTotalPrice = createdOrder.getTotalPrice();
		productService.updateProduct(productOne.getId(), new ProductModificationDTO(productOne.getName(), finalFirstItemQuantity));

		OrderDTO retrievedOrder = orderService.getOrder(createdOrder.getId());
		assertThat(retrievedOrder.getTotalPrice()).isEqualTo(originalTotalPrice);

		// when
		OrderDTO recalculatedOrder = orderService.recalculateOrder(createdOrder.getId());


		// then
		assertThat(recalculatedOrder.getTotalPrice()).isNotEqualTo(originalTotalPrice);
		assertThat(recalculatedOrder.getTotalPrice()).isEqualTo((productOne.getPrice().multiply(finalFirstItemQuantity)).add(productTwo.getPrice().multiply(secondItemQuantity)));


	}

	@Test
	void shouldFindOrders() {
		// setup
		Collection<OrderDTO> originalOrders = orderService.searchOrders(LocalDateTime.now().minusDays(1), LocalDateTime.now());
		assertThat(originalOrders).hasSize(0);

		OrderCreationDTO orderCreationDTO = OrderFixture.orderCreationWithTwoItems(productOne.getId(), BigDecimal.TEN,
				productTwo.getId(), BigDecimal.TEN);

		orderService.createOrder(orderCreationDTO);

		// when
		Collection<OrderDTO> orders = orderService.searchOrders(LocalDateTime.now().minusDays(1), LocalDateTime.now());

		// then
		assertThat(orders).hasSize(1);
	}

}
