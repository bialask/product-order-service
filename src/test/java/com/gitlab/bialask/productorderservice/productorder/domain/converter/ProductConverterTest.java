package com.gitlab.bialask.productorderservice.productorder.domain.converter;

import com.gitlab.bialask.productorderservice.productorder.domain.dto.ProductDTO;
import com.gitlab.bialask.productorderservice.productorder.domain.dto.ProductModificationDTO;
import com.gitlab.bialask.productorderservice.productorder.domain.fixtures.ProductFixture;
import com.gitlab.bialask.productorderservice.productorder.domain.model.Product;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ProductConverterTest {

	private ProductConverter converter = new ProductConverter();

	@Test
	void shouldConvertProduct() {
		// given
		Product product = Product.builder()
				.id(23L)
				.price(ProductFixture.TEST_PRICE)
				.name(ProductFixture.TEST_NAME).build();

		// when
		ProductDTO dto = converter.convert(product);

		// then
		assertThat(dto.getId()).isEqualTo(product.getId());
		assertThat(dto.getName()).isEqualTo(product.getName());
		assertThat(dto.getPrice()).isEqualTo(product.getPrice());

	}

	@Test
	void shouldConvertDTO() {
		// given
		ProductModificationDTO dto = ProductModificationDTO.builder()
				.price(ProductFixture.TEST_PRICE)
				.name(ProductFixture.TEST_NAME).build();

		// when
		Product product = converter.convert(dto);

		// then
		assertThat(dto.getName()).isEqualTo(product.getName());
		assertThat(dto.getPrice()).isEqualTo(product.getPrice());
	}

}
