package com.gitlab.bialask.productorderservice.productorder.domain.converter;

import com.gitlab.bialask.productorderservice.productorder.domain.dto.OrderCreationDTO;
import com.gitlab.bialask.productorderservice.productorder.domain.dto.OrderDTO;
import com.gitlab.bialask.productorderservice.productorder.domain.dto.OrderItemDTO;
import com.gitlab.bialask.productorderservice.productorder.domain.fixtures.ProductFixture;
import com.gitlab.bialask.productorderservice.productorder.domain.model.Order;
import com.gitlab.bialask.productorderservice.productorder.domain.fixtures.OrderFixture;
import com.gitlab.bialask.productorderservice.productorder.domain.model.OrderItem;
import com.gitlab.bialask.productorderservice.productorder.domain.model.Product;
import com.google.common.collect.Lists;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

public class OrderConverterTest {

	private OrderConverter converter = new OrderConverter();

	@Test
	void shouldConvertOrder() {
		// given
		Product product = Product.builder()
				.name(ProductFixture.TEST_NAME).id(23L).build();
		OrderItem orderItem = new OrderItem(BigDecimal.TEN, BigDecimal.ONE, product);
		Order order = Order.builder()
				.customerEmail(OrderFixture.TEST_EMAIL)
				.totalPrice(OrderFixture.TEST_PRICE)
				.createdDate(LocalDateTime.now())
				.orderItems(Lists.newArrayList(orderItem))
				.build();

		// when
		OrderDTO dto = converter.convert(order);

		// then
		assertThat(dto.getCustomerEmail()).isEqualTo(order.getCustomerEmail());
		assertThat(dto.getTotalPrice()).isEqualTo(order.getTotalPrice());
		assertThat(dto.getCreatedDate()).isEqualTo(order.getCreatedDate());
		OrderItemDTO firstItem = dto.getItems().get(0);
		assertThat(firstItem.getName()).isEqualTo(product.getName());
		assertThat(firstItem.getProductId()).isEqualTo(product.getId());
		assertThat(firstItem.getPrice()).isEqualTo(orderItem.getPurchasePrice());

	}

	@Test
	void shouldConvertDTO() {
		// given
		OrderCreationDTO dto = OrderCreationDTO.builder()
				.customerEmail(OrderFixture.TEST_EMAIL)
				.build();

		// when
		Order order = converter.convert(dto);

		// then
		assertThat(dto.getCustomerEmail()).isEqualTo(order.getCustomerEmail());
	}
}
