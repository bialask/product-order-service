package com.gitlab.bialask.productorderservice.productorder.domain.fixtures;

import com.gitlab.bialask.productorderservice.productorder.domain.dto.ProductModificationDTO;

import java.math.BigDecimal;

public class ProductFixture {

	public static final String TEST_NAME = "Tesla Model 3";
	public static final BigDecimal TEST_PRICE = BigDecimal.TEN;

	public static ProductModificationDTO productModificationOne() {
		return new ProductModificationDTO(TEST_NAME, TEST_PRICE);
	}

	public static ProductModificationDTO productModificationTwo() {
		return new ProductModificationDTO("Tesla S", BigDecimal.ONE);
	}
}
