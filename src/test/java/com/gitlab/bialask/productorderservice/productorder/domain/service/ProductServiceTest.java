package com.gitlab.bialask.productorderservice.productorder.domain.service;


import com.gitlab.bialask.productorderservice.productorder.domain.converter.ProductConverter;
import com.gitlab.bialask.productorderservice.productorder.domain.dto.ProductDTO;
import com.gitlab.bialask.productorderservice.productorder.domain.dto.ProductModificationDTO;
import com.gitlab.bialask.productorderservice.productorder.domain.fixtures.ProductFixture;
import com.gitlab.bialask.productorderservice.productorder.domain.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ExtendWith(SpringExtension.class)
class ProductServiceTest {

	@Autowired
	private ProductRepository productRepository;

	private ProductService productService;

	@BeforeEach
	void setUp() {
		productService = new ProductService(productRepository,new ProductConverter());
	}
	//
	@Test
	void shouldCreateProduct() {
		// given
		final ProductModificationDTO dto = ProductFixture.productModificationOne();

		// when
		final ProductDTO product = productService.createProduct(dto);

		// then
		assertThat(productService.findAllProducts().size()).isGreaterThan(0);

		assertThat(product.getPrice()).isEqualTo(dto.getPrice());
		assertThat(product.getName()).isEqualTo(dto.getName());
	}

    @Test
    void shouldUpdateProduct() {
        // setup
        final ProductDTO productOne = productService.createProduct(ProductFixture.productModificationOne());

        // given
        final ProductModificationDTO modificationDTO = ProductFixture.productModificationTwo();

        //when
        final ProductDTO productTwo = productService.updateProduct(productOne.getId(), modificationDTO);

        // then
	    assertThat(productTwo.getId()).isEqualTo(productOne.getId());
	    assertThat(productTwo.getPrice()).isEqualTo(modificationDTO.getPrice());
	    assertThat(productTwo.getName()).isEqualTo(modificationDTO.getName());
    }

}
